CC := gcc
SRCDIR := src
BUILDDIR := build
TARGET := bin/encrypt
TESTDIR := test

all: build

test: build
	cd $(TESTDIR) && ../$(TARGET) && cat *-message

# build: $(SRCDIR)/*.c $(SRCDIR)/include/*.h
build: $(SRCDIR)/*.c
	cd $(BUILDDIR) && gcc -c ../$(SRCDIR)/*.c
	cd ..
	gcc -o "$(BUILDDIR)/binary" $(BUILDDIR)/*.o
	mv $(BUILDDIR)/binary $(TARGET)

clean:
	rm -f $(BUILDDIR)/*
	rm -f $(TARGET)
	rm -f $(TESTDIR)/*-message

#include <stdio.h>
#include <stdlib.h>

char *readfile(char *path){
  FILE *fptr;
  char *buff = calloc(256, sizeof(char));
  
  fptr = fopen(path,"r");
  if(!fptr){
    printf("Error!");
    exit(1);
  }

  fgets(buff, 255, (FILE*)fptr);
  fclose(fptr);

  return buff;
}

void writefile(char *path, char *str){
  FILE *fptr;
  fptr = fopen(path, "w");

  fputs(str, fptr);
  fputc('\n', fptr);

  fclose(fptr);
}

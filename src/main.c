#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "include/file.h"
#include "include/encrypt.h"

int main(){

  // READING
  char *message = readfile("message");
  
  // ENCRYPTION 
  char *encrypted = (char*) calloc(strlen(message), sizeof(char));
  encrypt(message, encrypted);
  writefile("encrypted-message", encrypted);

  // DECRYPTION
  char *decrypted = (char*) calloc(strlen(message), sizeof(char));
  decrypt(encrypted, decrypted);
  writefile("decrypted-message", decrypted);

  free(decrypted);
  free(encrypted);
  free(message);

  return 0;
}

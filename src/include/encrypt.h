#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void encrypt(char *buf, char *encrypted);
void decrypt(char *encrypted, char *decrypted);

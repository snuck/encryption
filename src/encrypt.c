#include <string.h>

char encrypt_algo(char c){
  return c+4;
}

char decrypt_algo(char c){
  return c-4;
}

void encrypt(char *message, char *encrypted){
  int i;
  for(i = 0; i < strlen(message)-1 ; i++){
    encrypted[i] = encrypt_algo(message[i]);
  }
}

void decrypt(char *encrypted, char *decrypted){
  int i;
  for(i = 0; i < strlen(encrypted) ; i++){
    decrypted[i] = decrypt_algo(encrypted[i]);
  }
}
